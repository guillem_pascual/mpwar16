<?php
/**
 * Created by PhpStorm.
 * User: guillempascual
 * Date: 30/4/16
 * Time: 19:52
 */

namespace MpwarUnit\SignUp;


use InvalidArgumentException;
use Mpwar\SignUp\RegisterUser;
use Mpwar\SignUp\UserRepository;

class RegisterUserTest extends \PHPUnit_Framework_TestCase
{

    const INVALID_EMAIL_ADDRESS = 'guillempascual@';
    const VALID_EMAIL_ADDRESS = 'guillempascual@';
    const INVALID_PASSWORD = 'aPassword';
    const VALID_PASSWORD = 'AP4ssw0rd';

    private $email;
    private $password;

    /** @test */

    public function shouldThrowException()
    {
        $this->givenAnInvalidEmailAddress();
        $this->andAValidPassword();
        $this->thenAnExceptionShoulBeThrown();
        $this->whenRegisteringAUser();
    }

    private function givenAnInvalidEmailAddress()
    {
        $this->email = self::INVALID_EMAIL_ADDRESS;
    }

    private function andAValidPassword()
    {
        $this->password = self::VALID_PASSWORD;
    }

    private function thenAnExceptionShoulBeThrown()
    {
        $this->expectException(InvalidArgumentException::class);
    }

    private function whenRegisteringAUser()
    {
        $userRepository = $this->getMock(UserRepository::class);
        $registerUser = new RegisterUser($userRepository);
        $registerUser->registerNew(self::INVALID_EMAIL_ADDRESS,self::VALID_PASSWORD);
    }

}