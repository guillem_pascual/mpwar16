<?php

namespace MpwarUnit\Doubles;

use PHPUnit_Framework_TestCase;
use Mpwar\Doubles\AnotherService;
use Mpwar\Doubles\PrimeNumberService;
use Mpwar\Doubles\Sut;

final class SutTest extends PHPUnit_Framework_TestCase
{
    const NEGATIVE_NUMBER = 'negative';
    const PRIME_NUMBER = 'prime';
    const FOO_BAR_NUMBER = 'foobar';

    /** @test */
    public function shouldReturnNegative()
    {
        $primeNumberServiceDummy = $this->getMock(PrimeNumberService::class);
        $anotherServiceDummy = $this->getMock(AnotherService::class);
        $sut = new Sut($primeNumberServiceDummy, $anotherServiceDummy);

        $this->assertEquals(self::NEGATIVE_NUMBER, $sut->doSomething(-1));
    }

    /** @test */
    public function shouldReturnPrime()
    {
        $primeNumberServiceStub = $this->getMock(PrimeNumberService::class);
        $anotherServiceDummy = $this->getMock(AnotherService::class);
        $sut = new Sut($primeNumberServiceStub, $anotherServiceDummy);

        $primeNumberServiceStub
            ->method('isPrime')
            ->will($this->returnValue(true));

        $this->assertEquals(self::PRIME_NUMBER, $sut->doSomething(7));
    }

    /** @test */
    public function shouldReturnAnotherNumberType()
    {
        $number = 24;
        $primeNumberServiceStub = $this->getMock(PrimeNumberService::class);
        $anotherServiceMock = $this->getMock(AnotherService::class);
        $sut = new Sut($primeNumberServiceStub, $anotherServiceMock);

        $primeNumberServiceStub
            ->method('isPrime')
            ->will($this->returnValue(false));

        $anotherServiceMock
            ->expects($this->once())
            ->method('getNumberType')
            ->with($number)
            ->will($this->returnValue(self::FOO_BAR_NUMBER));

        $this->assertEquals(self::FOO_BAR_NUMBER, $sut->doSomething($number));
    }
}
