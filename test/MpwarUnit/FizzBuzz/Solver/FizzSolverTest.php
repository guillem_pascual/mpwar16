<?php

namespace MpwarUnit\FizzBuzz\Solver;

use Mpwar\FizzBuzz\Solver\FizzSolver;
use PHPUnit_Framework_TestCase;

final class FizzSolverTest extends PHPUnit_Framework_TestCase
{
    const FIZZ = 'fizz';
    const EMPTY_STACKED_RESULT = '';
    const FILLED_UP_STACKED_RESULT = 'monesvol';

    protected function tearDown()
    {
        $this->number = null;
        $this->stackedResult = null;
    }

    /**
     * @test
     * @dataProvider numberDivisibleByThreeProvider
     */
    public function shouldReturnFizzForNumbersThanCanBeDividedByThreeWithoutStackedResult($number)
    {
        $this->givenTheNumber($number);
        $this->andAnEmptyStackedResult();
        $this->thenTheResultIsFizz();
    }

    /**
     * @test
     * @dataProvider numberDivisibleByThreeProvider
     */
    public function shouldReturnFizzForNumbersThanCanBeDividedByThreeWithStackedResult($number)
    {
        $this->givenTheNumber($number);
        $this->andStackedResultWithAContent();
        $this->thenTheResultIsTheStackedResultPlusFizz();
    }

    /**
     * @test
     * @dataProvider numberIsNotDivisibleByThreeProvider
     */
    public function shouldReturnTheStackedResultWhenTheNumberIsNotDivisibleByThree($number)
    {
        $this->givenTheNumber($number);
        $this->andStackedResultWithAContent();
        $this->thenTheResultIsTheStackedResult();
    }

    public function numberDivisibleByThreeProvider()
    {
        return [
            [3], [6], [9], [12], [15], [18], [21],
        ];
    }

    public function numberIsNotDivisibleByThreeProvider()
    {
        return [
            [2], [4], [5],
        ];
    }

    private function givenTheNumber($number)
    {
        $this->number = $number;
    }

    private function andAnEmptyStackedResult()
    {
        $this->stackedResult = self::EMPTY_STACKED_RESULT;
    }

    private function andStackedResultWithAContent()
    {
        $this->stackedResult = self::FILLED_UP_STACKED_RESULT;
    }

    private function thenTheResultIsFizz()
    {
        $result = $this->executeService();
        $this->assertSame(self::FIZZ, $result);
    }

    private function thenTheResultIsTheStackedResultPlusFizz()
    {
        $result = $this->executeService();
        $this->assertSame(
            self::FILLED_UP_STACKED_RESULT . ' ' . self::FIZZ,
            $result
        );
    }

    private function thenTheResultIsTheStackedResult()
    {
        $result = $this->executeService();
        $this->assertSame(self::FILLED_UP_STACKED_RESULT, $result);
    }

    private function executeService()
    {
        $fizzSolver = new FizzSolver;

        return $fizzSolver->composeStackedResult($this->number, $this->stackedResult);
    }
}