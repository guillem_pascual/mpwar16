Feature: I can detect generic numbers
    As a sane person
    I want to make sure that generic numbers can be recognised
    So that I can pass testing

    Scenario: default case
        Given a 2
        When executing the fizz buzz service
        Then the result should be '2'

    Scenario: another case
        Given a 37
        When executing the fizz buzz service
        Then the result should be '37'
