<?php
/**
 * Created by PhpStorm.
 * User: guillempascual
 * Date: 30/4/16
 * Time: 19:45
 */

namespace Mpwar\SignUp;


use SebastianBergmann\GlobalState\Exception;

class RegisterUser
{
    const MIN_PASSWORD_LENGTH = 6;

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function registerNew($email, $password)
    {
        if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
            throw new \InvalidArgumentException('Not valid email');
        }

        if(!(strlen($password) >= self::MIN_PASSWORD_LENGTH) && preg_match( '/[A-Z]/', $password, null, PREG_OFFSET_CAPTURE ) && preg_match( '/[0-9]/', $password, null, PREG_OFFSET_CAPTURE )){
            throw new \InvalidArgumentException('Not valid password');
        }
         return null;
    }

}