<?php
/**
 * Created by PhpStorm.
 * User: guillempascual
 * Date: 30/4/16
 * Time: 19:57
 */

namespace Mpwar\SignUp;


interface UserRepository
{
    public function addUser($email,$password);
}